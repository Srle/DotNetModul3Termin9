﻿$(document).ready(function () {

    // podaci od interesa
    var host = window.location.host;
    var proizvodEndpoint = "/api/proizvod/";
    var formAction = "Create";
    var editingId;
    var token = null;
    var headers = {};

    // posto inicijalno nismo prijavljeni, sakrivamo odjavu
    $("#odjava").css("display", "none");
    //sakriva meni sa akcijama za proizvod
    $("#akcije").css("display", "none");

    //ispisuje inicijalnu tabelu proizvoda
    ispisisve();

    // registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz2").val();

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };

        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            $("#info").append("Uspešna registracija. Možete se prijaviti na sistem.");

        }).fail(function (data) {
            alert(data);
        });


    });


    // prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);

            //Briše tabelu proizvoda
            $("#sadrzaj").empty();

            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            token = data.access_token;
            $("#prijava").css("display", "none");
            $("#registracija").css("display", "none");
            $("#odjava").css("display", "block");
            $("#akcije").css("display", "block");
            //////////////////////////////////////////////////////////////////////

            // prikaz proizvoda
            $("#btnakcije").click(function () {
                // ucitavanje proizvoda
                var requestUrl = 'http://' + host + proizvodEndpoint;
                console.log("URL zahteva: " + requestUrl);
                $.getJSON(requestUrl, setProizvodi);
            });

            // metoda za postavljanje proizvoda u tabelu
            function setProizvodi(data, status) {
                console.log("Status: " + status);

                var $container = $("#data");
                $container.empty();

                $container.append(div);

                if (status == "success") {
                    console.log(data);
                    // ispis naslova
                    var div = $("<div></div>");
                    var h1 = $("<h1>Proizvodi</h1>");
                    div.append(h1);
                    // ispis tabele proizvoda
                    var table = $("<table  class='table table-hover table-bordered'></table>");
                    var header = $("<tr><td>Id</td><td>Ime</td><td>Cena</td><td>Datum izradr</td><td>Delete</td><td>Edit</td></tr>");
                    table.append(header);
                    for (i = 0; i < data.length; i++) {
                        // prikazujemo novi red u tabeli
                        var row = "<tr>";
                        // prikaz podataka
                        var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Name + "</td><td>" + data[i].Price + "</td><td>" + data[i].Date + "</td>";
                        // prikaz dugmadi za izmenu i brisanje
                        var stringId = data[i].Id.toString();
                        var displayDelete = "<td><button id=btnDeleteProizvod name=" + stringId + ">Delete</button></td>";
                        var displayEdit = "<td><button id=btnEditProizvod name=" + stringId + ">Edit</button></td>";
                        row += displayData + displayDelete + displayEdit + "</tr>";
                        table.append(row);
                    }

                    div.append(table);

                    // prikaz forme
                    $("#formDiv1").css("display", "block");

                    // ispis novog sadrzaja
                    $container.append(div);
                }
                else {
                    var div = $("<div></div>");
                    var h1 = $("<h1>Greška prilikom preuzimanja svih proizvoda!</h1>");
                    div.append(h1);
                    $container.append(div);
                }
            };

            // dodavanje novog proizvoda
            $("#formProizvod").submit(function (e) {
                // sprecavanje default akcije forme
                e.preventDefault();

                var proizvodIme = $("#imeProizvod").val();
                var proizvodCena = $("#cenaProizvod").val();
                var proizvodDate = $("#dateProizvod").val();
                var httpAction;
                var sendData;
                var url;

                // u zavisnosti od akcije pripremam objekat režiser
                if (formAction === "Create") {
                    httpAction = "POST";
                    url = 'http://' + host + proizvodEndpoint;
                    sendData = {
                        "Name": proizvodIme,
                        "Price": proizvodCena,
                        "Date": proizvodDate
                    };
                }
                else {
                    httpAction = "PUT";
                    url = 'http://' + host + proizvodEndpoint + editingId.toString();
                    sendData = {
                        "Id": editingId,
                        "Name": proizvodIme,
                        "Price": proizvodCena,
                        "Date": proizvodDate
                    };
                }

                console.log("Objekat za slanje:");
                console.log(sendData);

                if (token) { headers.Authorization = 'Bearer ' + token; }

                $.ajax({
                    url: url,
                    type: httpAction,
                    data: sendData,
                    headers: headers
                })
                    .done(function (data, status) {
                        formAction = "Create";
                        refreshTable();
                    })
                    .fail(function (data, status) {
                        alert("Desila se greska!");
                    })

            });

            // pripremanje dogadjaja za brisanje proizvoda
            $("body").on("click", "#btnDeleteProizvod", deleteProizvod);

            // priprema dogadjaja za izmenu proizvoda
            $("body").on("click", "#btnEditProizvod", editProizvod);

            // brisanje proizvoda
            function deleteProizvod() {
                // izvlacimo {id}
                var deleteID = this.name;
                // saljemo zahtev 

                if (token) { headers.Authorization = 'Bearer ' + token; }

                $.ajax({
                    url: 'http://' + host + proizvodEndpoint + deleteID.toString(),
                    type: "DELETE",
                    headers: headers
                })
                    .done(function (data, status) {
                        refreshTable();
                    })
                    .fail(function (data, status) {
                        alert("Desila se greska!");
                    });

            };

            // izmena proizvoda
            function editProizvod() {
                // izvlacimo id
                var editId = this.name;
                // saljemo zahtev da dobavimo taj proizvod

                if (token) { headers.Authorization = 'Bearer ' + token; }

                $.ajax({
                    url: 'http://' + host + proizvodEndpoint + editId.toString(),
                    type: "GET",
                    headers: headers
                })
                    .done(function (data, status) {
                        $("#imeProizvod").val(data.Name);
                        $("#cenaProizvod").val(data.Price);
                        $("#dateProizvod").val(data.Date);
                        editingId = data.Id;
                        formAction = "Update";
                    })
                    .fail(function (data, status) {
                        formAction = "Create";
                        alert("Desila se greska!");
                    });

            };

            // osvezi prikaz tabele proizvoda
            function refreshTable() {
                // cistim formu
                $("#imeProizvod").val('');
                $("#cenaProizvod").val('');
                $("#dateProizvod").val('');
                // osvezavam
                $("#btnakcije").trigger("click");
            };

          /////////////////////////////////////////////////////////////////////////
        }).fail(function (data) {
            alert(data);
        });
    });

    // odjava korisnika sa sistema
    $("#odjavise").click(function () {
        token = null;
        headers = {};

        $("#prijava").css("display", "block");
        $("#registracija").css("display", "block");
        $("#odjava").css("display", "none");
        $("#info").empty();
        $("#sadrzaj").empty();
        //sakriva meni sa akcijama za proizvod
        $("#akcije").css("display", "none");

        //ispisuje inicijalnu tabelu proizvoda
        ispisisve();

    })

    // ucitavanje svih proizvoda
    function ispisisve () {

        console.log("URL zahteva: " + "http://" + host + proizvodEndpoint);
        $.ajax({
            "type": "GET",
            "url": "http://" + host + proizvodEndpoint,
            "dataType": "JSON",
            "headers": headers

        }).done(function (data) {
            var Prikaz = $("#sadrzaj");
            Prikaz.empty()

            // ispis naslova
            var div = $("<div></div>");
            var h3 = $("<h3>Proizvodi</h3>");
            div.append(h3);
            // ispis tabele proizvoda
            var table = $("<table class='table table-dark table-striped'></table>");
            var header = $("<tr><td>Id</td><td>Ime</td><td>Cena</td><td>Datum</td></tr>");
            table.append(header);
            for (i = 0; i < data.length; i++) {
                // prikazujemo novi red u tabeli
                var row = "<tr>";
                // prikaz podataka
                var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Name + "</td><td>" + data[i].Price + "</td><td>" + data[i].Date + "</td>";
                row += displayData + "</tr>";
                table.append(row);
            }

            $(table).addClass('table table-hover table-bordered');

            div.append(table);

            // ispis novog sadrzaja
            Prikaz.append(div);

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });


    };


});