﻿using RadSaProizvodima.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace RadSaProizvodima.Controllers
{
    //Traži autorizaciju za sve akcije kontrolera (osim GetProizvodi)
    [Authorize]
    public class ProizvodController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //GET api/proizvod
        //Dozvoljava pristup bez autorizacije
        [AllowAnonymous]
        public IQueryable<Proizvod> GetProizvodi()
        {
            return db.Proizvodi;
        }


        //GET api/proizvod/1
        [ResponseType(typeof(Proizvod))]
        public IHttpActionResult GetProizvod(int id)
        {
            Proizvod proizvod = db.Proizvodi.Find(id);
            if (proizvod == null)
            {
                return NotFound();
            }

            return Ok(proizvod);
        }

        //POST api/proizvod
        [ResponseType(typeof(Proizvod))]
        public IHttpActionResult PostAuthor(Proizvod proizvod)
        {
            if (!ModelState.IsValid)

            {
                return BadRequest(ModelState);
            }
            db.Proizvodi.Add(proizvod);
            db.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = proizvod.Id }, proizvod);

        }

        private bool ProizvodExists(int id)
        {
            return db.Proizvodi.Count(e => e.Id == id) > 0;
        }

        // PUT api/proizvod/1
        [ResponseType(typeof(Proizvod))]
        public IHttpActionResult PutProizvod(int id, Proizvod proizvod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != proizvod.Id)
            {
                return BadRequest();
            }

            db.Entry(proizvod).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProizvodExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(proizvod);
        }


        // DELETE api/proizvod/1
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteProizvod(int id)
        {
            Proizvod proizvod = db.Proizvodi.Find(id);
            if (proizvod == null)
            {
                return NotFound();
            }

            db.Proizvodi.Remove(proizvod);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }





    }
}
