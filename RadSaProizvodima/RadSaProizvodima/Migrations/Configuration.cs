namespace RadSaProizvodima.Migrations
{
    using RadSaProizvodima.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RadSaProizvodima.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RadSaProizvodima.Models.ApplicationDbContext context)
        {
            context.Proizvodi.AddOrUpdate(x => x.Id,
                new Proizvod() { Id = 1, Name = "Proizvod 1", Price = 10, Date = new DateTime(2015, 5, 15)  },
                new Proizvod() { Id = 2, Name = "Proizvod 2", Price = 20, Date = new DateTime (2017, 1, 25) },
                new Proizvod() { Id = 3, Name = "Proizvod 3", Price = 30, Date = new DateTime(2018, 08, 09) }
                );
        }
    }
}
